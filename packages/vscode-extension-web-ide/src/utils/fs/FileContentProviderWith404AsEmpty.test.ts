import type { IFileContentProvider } from '@gitlab/web-ide-fs';
import { FileContentProviderWith404AsEmpty } from './FileContentProviderWith404AsEmpty';

const TEST_CONTENT = Buffer.from('Hello world!');
const TEST_PATH = 'foo/path';

describe('utils/fs/FileContentProviderWith404AsEmpty', () => {
  let base: IFileContentProvider;
  let subject: FileContentProviderWith404AsEmpty;

  const createSubject = () => new FileContentProviderWith404AsEmpty(base);

  beforeEach(() => {
    base = {
      getContent: jest.fn().mockResolvedValue(TEST_CONTENT),
    };
    subject = createSubject();
  });

  it('when base resolves, passes through', async () => {
    await expect(subject.getContent(TEST_PATH)).resolves.toBe(TEST_CONTENT);

    expect(base.getContent).toHaveBeenCalledWith(TEST_PATH);
  });

  it('when base errors with 404, returns empty', async () => {
    jest.mocked(base.getContent).mockRejectedValue(new Error('something 404 something'));

    await expect(subject.getContent(TEST_PATH)).resolves.toEqual(new Uint8Array(0));
  });

  it('when base errors catastrophically, passes throw', async () => {
    const error = new Error('Something really bad');
    jest.mocked(base.getContent).mockRejectedValue(error);

    await expect(subject.getContent(TEST_PATH)).rejects.toBe(error);
  });
});
