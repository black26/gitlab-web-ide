import 'whatwg-fetch';
import { createResponseError } from './createResponseError';

describe('createResponseError', () => {
  describe('default', () => {
    it.each`
      description                                     | contentType           | body                  | error
      ${'handles an error response with a json body'} | ${'application/json'} | ${'{ "bar": "foo" }'} | ${new Error('{"status":400,"body":{"bar":"foo"}}')}
      ${'handles an error response with a text body'} | ${'text/plain'}       | ${'bar foo'}          | ${new Error('{"status":400,"body":"bar foo"}')}
    `('$description', async ({ contentType, body, error }) => {
      expect(
        await createResponseError(
          new Response(body, {
            headers: new Headers({ 'Content-Type': contentType }),
            status: 400,
          }),
        ),
      ).toEqual(error);
    });
  });
});
