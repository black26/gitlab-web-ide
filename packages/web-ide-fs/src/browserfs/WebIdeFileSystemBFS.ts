import { joinPaths } from '@gitlab/utils-path';
import { FileFlag } from 'browserfs/dist/node/core/file_flag';
import { FileType, IFileStats, IFileSystem } from '../types';
import { IPromisifiedBrowserFS } from './types';
import { IDeletedFilesLogReadonly } from './typesOverlayFS';
import { convertToFileStats } from './utils';

export interface WebIdeFileSystemBFSOptions {
  // This is the main OverlayFS
  fs: IPromisifiedBrowserFS;

  // This is the deletedFilesLog we also need to calculate The-Real-Modified-Time TM
  deletedFilesLog: IDeletedFilesLogReadonly;

  // Repo root path is needed to calculate The-Real-Modified-Time TM
  repoRootPath: string;
}

export class WebIdeFileSystemBFS implements IFileSystem {
  private readonly _fs: IPromisifiedBrowserFS;

  private readonly _deletedFilesLog: IDeletedFilesLogReadonly;

  private readonly _repoRootPath: string;

  constructor({ fs, deletedFilesLog, repoRootPath }: WebIdeFileSystemBFSOptions) {
    this._fs = fs;
    this._deletedFilesLog = deletedFilesLog;
    this._repoRootPath = repoRootPath;
  }

  async lastModifiedTime(): Promise<number> {
    const repoRootStat = await this._fs.stat(joinPaths('/', this._repoRootPath), false);
    const repoMTime = repoRootStat.mtime.getTime();
    const deletedMTime = await this._deletedFilesLog.getModifiedTime();

    return Math.max(repoMTime, deletedMTime);
  }

  async stat(path: string): Promise<IFileStats> {
    const stat = await this._fs.stat(path, false);

    return convertToFileStats(stat);
  }

  readdir(path: string): Promise<string[]> {
    return this._fs.readdir(path);
  }

  async readdirWithTypes(path: string): Promise<[string, FileType][]> {
    const children = await this._fs.readdir(path);

    const result: Promise<[string, FileType]>[] = children.map(name => {
      const childPath = joinPaths(path, name);

      return this.stat(childPath).then(stat => [name, stat.type]);
    });

    return Promise.all(result);
  }

  mkdir(path: string): Promise<void> {
    // TODO: Is 0 for the mode really safe?
    return this._fs.mkdir(path, 0);
  }

  async readFile(path: string): Promise<Uint8Array> {
    const content = await this._fs.readFile(path, null, FileFlag.getFileFlag('r'));

    // We know this will be a Buffer because we passed "null" for "utf-8"
    return <Buffer>content;
  }

  writeFile(path: string, data: Uint8Array): Promise<void> {
    return this._fs.writeFile(path, Buffer.from(data), null, FileFlag.getFileFlag('w'), 0);
  }

  rename(oldPath: string, newPath: string): Promise<void> {
    return this._fs.rename(oldPath, newPath);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async rm(path: string, opt: { recursive: boolean }): Promise<void> {
    const stat = await this.stat(path);

    return this._rmrf(path, stat.type);
  }

  // eslint-disable-next-line consistent-return
  private async _rmrf(path: string, fileType: FileType): Promise<void> {
    if (fileType === FileType.Blob) {
      return this._removeFile(path);
    }

    const children = await this.readdirWithTypes(path);
    const childrenRemoved = children.map(([name, type]) => {
      const childPath = joinPaths(path, name);

      return this._rmrf(childPath, type);
    });
    await Promise.all(childrenRemoved);

    await this._removeDir(path);
  }

  private _removeDir(path: string): Promise<void> {
    return this._fs.rmdir(path);
  }

  private _removeFile(path: string): Promise<void> {
    return this._fs.unlink(path);
  }
}
