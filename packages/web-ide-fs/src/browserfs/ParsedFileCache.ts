import { FileFlag } from 'browserfs/dist/node/core/file_flag';
import { IReadonlyPromisifiedBrowserFS } from './types';

interface Parser<T> {
  (content: string): Promise<T>;
}

export class ParsedFileCache<T> {
  private readonly _fs: IReadonlyPromisifiedBrowserFS;

  private readonly _filePath: string;

  private readonly _parser: Parser<T>;

  // Cache is either a tuple of (cacheKey, cacheValue) or it is empty
  private _cache: [string, T] | undefined;

  constructor(fs: IReadonlyPromisifiedBrowserFS, filePath: string, parser: Parser<T>) {
    this._fs = fs;
    this._filePath = filePath;
    this._parser = parser;
  }

  public async getContents(): Promise<T | null> {
    const cacheKey = await this._getCacheKey();

    if (!cacheKey) {
      return null;
    }
    if (this._cache && cacheKey === this._cache[0]) {
      return this._cache[1];
    }

    const value = await this._parseFileContents();

    this._cache = [cacheKey, value];

    return value;
  }

  private async _getCacheKey(): Promise<string | null> {
    try {
      const stat = await this._fs.stat(this._filePath, null);

      // why: Using just mtime.getTime() can be flaky in some edge cases.
      //      In unit tests, sometimes the time between 2 write operations
      //      can be less than a ms which wouldn't trigger a cache invalidation.
      return `${stat.size}_${stat.mtime.getTime()}`;
    } catch (e) {
      // The path was not found! Let's just assume empty.
      return null;
    }
  }

  private async _parseFileContents(): Promise<T> {
    // We are guaranteed this is a "string" because we passed the encoding
    const content = <string>(
      await this._fs.readFile(this._filePath, 'utf-8', FileFlag.getFileFlag('r'))
    );

    return this._parser(content);
  }
}
