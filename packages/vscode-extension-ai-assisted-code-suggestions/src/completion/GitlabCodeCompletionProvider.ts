import * as vscode from 'vscode';
import { AI_ASSISTED_CODE_SUGGESTIONS_API_URL } from '../constants';
import {
  getAiAssistedCodeSuggestionsConfiguration,
  AI_ASSISTED_CODE_SUGGESTIONS_MODE,
  AI_ASSISTED_CODE_SUGGESTIONS_CONFIG,
} from '../utils/extensionConfiguration';
import { getStopSequences } from '../utils/getStopSequences';

export class GitLabCodeCompletionProvider implements vscode.InlineCompletionItemProvider {
  private model: string;

  private server: string;

  private debouncedCall: ReturnType<typeof setTimeout> | undefined;

  private debounceTimeMs = 500;

  private noDebounce: boolean;

  constructor(noDebounce = false) {
    this.model = 'gitlab';
    this.server = GitLabCodeCompletionProvider.#getServer();
    this.debouncedCall = undefined;
    this.noDebounce = noDebounce;
  }

  static registerGitLabCodeCompletion(context: vscode.ExtensionContext) {
    let subscription: vscode.Disposable | undefined;
    const dispose = () => subscription?.dispose();
    context.subscriptions.push({ dispose });

    const register = () => {
      subscription = vscode.languages.registerInlineCompletionItemProvider(
        { pattern: '**' },
        new GitLabCodeCompletionProvider(),
      );
    };
    if (getAiAssistedCodeSuggestionsConfiguration().enabled) {
      register();
    }

    vscode.workspace.onDidChangeConfiguration(e => {
      if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_MODE)) {
        if (!getAiAssistedCodeSuggestionsConfiguration().enabled) {
          dispose();
        } else {
          register();
        }
      } else if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_CONFIG)) {
        dispose();
        register();
      }
    });
  }

  static #getServer(): string {
    const serverUrl = new URL(AI_ASSISTED_CODE_SUGGESTIONS_API_URL);
    return serverUrl.href.toString();
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async fetchCompletions(params = {}): Promise<any> {
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    };
    const response = await fetch(this.server, requestOptions);
    if (!response.ok) {
      return [];
    }
    const data = await response.json();
    return data;
  }

  async getCompletions(document: vscode.TextDocument, position: vscode.Position) {
    const prompt = document.getText(new vscode.Range(0, 0, position.line, position.character));

    if (!prompt) {
      return [] as vscode.InlineCompletionItem[];
    }

    const data = await this.fetchCompletions({
      model: this.model,
      prompt,
      stop: getStopSequences(position.line, document),
    });

    return (
      data.choices?.map(
        (choice: { text: string }) =>
          new vscode.InlineCompletionItem(choice.text, new vscode.Range(position, position)),
      ) || []
    );
  }

  async provideInlineCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    context: vscode.InlineCompletionContext,
  ): Promise<vscode.InlineCompletionItem[]> {
    clearTimeout(this.debouncedCall);

    return new Promise(resolve => {
      //  In case of a hover, this will be triggered which is not desired as it calls for a new prediction
      if (context.triggerKind === vscode.InlineCompletionTriggerKind.Automatic) {
        if (this.noDebounce) {
          resolve(this.getCompletions(document, position));
        } else {
          this.debouncedCall = setTimeout(() => {
            resolve(this.getCompletions(document, position));
          }, this.debounceTimeMs);
        }
      }
    });
  }
}
