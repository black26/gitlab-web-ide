import vscode from 'vscode';
import { GitLabCodeCompletionProvider } from './completion/GitlabCodeCompletionProvider';

export async function activate(context: vscode.ExtensionContext) {
  GitLabCodeCompletionProvider.registerGitLabCodeCompletion(context);
}
