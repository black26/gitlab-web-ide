export const CONFIG_NAMESPACE = 'gitlab';
export const AI_ASSISTED_CODE_SUGGESTIONS_CONFIG_NAMESPACE = 'gitlab.aiAssistedCodeSuggestions';
export const AI_ASSISTED_CODE_SUGGESTIONS_API_URL =
  'https://codesuggestions.gitlab.com/v1/completions';
