import { ConfigType, AnyConfig } from '@gitlab/web-ide-types';
import { createClientOnlyConfig, createRemoteHostConfig } from '@gitlab/utils-test';
import { getIframeHtml } from './getIframeHtml';

const clientOnlyConfig = createClientOnlyConfig();
const remoteHostConfig = createRemoteHostConfig();

describe('getIframeHtml', () => {
  const collectAttributes = (el: Element): Record<string, string> =>
    Array.from(el.attributes).reduce(
      (acc, attr) => Object.assign(acc, { [attr.name]: attr.value }),
      {},
    );

  // In a friendly school bus driver sort of way...
  // Not a Pennywise-The-Clown-From-It way
  const collectChildren = (el: Element) =>
    Array.from(el.children)
      .map(child => ({
        tag: child.tagName.toLowerCase(),
        attributes: collectAttributes(child),
        content: child.innerHTML.trim(),
      }))
      .reduce((acc: Record<string, Record<string, string>[]>, { tag, attributes, content }) => {
        acc[tag] = acc[tag] ?? [];

        if (Object.entries(attributes).length) {
          acc[tag].push(attributes);
        }

        if (content) {
          acc[tag].push({ content });
        }

        return acc;
      }, {});

  const createTestSubject = (configType: ConfigType, config: AnyConfig) => {
    const html = getIframeHtml(configType, config);

    const parser = new DOMParser();
    const document = parser.parseFromString(html, 'text/html');

    return {
      head: collectChildren(document.head),
      body: collectChildren(document.body),
    };
  };

  type TestParameters = {
    configType: ConfigType;
    config: AnyConfig;
    scriptAttrs: Record<string, string>;
  };

  it.each`
    desc                           | configType       | config                                         | scriptAttrs
    ${'client only config'}        | ${'client-only'} | ${clientOnlyConfig}                            | ${{}}
    ${'remote config'}             | ${'remote'}      | ${remoteHostConfig}                            | ${{}}
    ${'with nonce'}                | ${'client-only'} | ${{ ...clientOnlyConfig, nonce: 'noncense' }}  | ${{ nonce: 'noncense' }}
    ${'with erroneous configType'} | ${'test"123'}    | ${clientOnlyConfig}                            | ${{}}
    ${'with erroneous baseUrl'}    | ${'client-only'} | ${{ ...clientOnlyConfig, baseUrl: '.."123.' }} | ${{}}
  `(
    'with $desc, returns properly escaped head and body elements',
    ({ configType, config, scriptAttrs }: TestParameters) => {
      const subject = createTestSubject(configType, config);

      expect(subject).toStrictEqual({
        head: {
          meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: expect.any(String) },
            { id: 'gl-config-type', 'data-settings': configType },
            { id: 'gl-config-json', 'data-settings': JSON.stringify(config) },
          ],
          link: [
            {
              'data-name': 'vs/workbench/workbench.web.main',
              rel: 'stylesheet',
              href: `${config.baseUrl}/vscode/out/vs/workbench/workbench.web.main.css`,
            },
          ],
        },
        body: {
          script: [
            {
              src: `${config.baseUrl}/main.js`,
              ...scriptAttrs,
            },
          ],
        },
      });
    },
  );

  it('injects the fonts if they are configured', () => {
    const subject = createTestSubject('client-only', {
      ...clientOnlyConfig,
      editorFont: {
        fontFamily: 'JetBrains Mono',
        format: 'woff2',
        srcUrl: 'http://example.com/fonts/JetBrainsMono.woff2',
      },
    });

    expect(subject.head.link[1]).toStrictEqual({
      rel: 'preload',
      as: 'font',
      type: 'font/woff2',
      crossorigin: '',
      href: `http://example.com/fonts/JetBrainsMono.woff2`,
    });

    expect(subject.head.style[0].content).toContain(`font-family: 'JetBrains Mono';`);
    expect(subject.head.style[0].content).toContain(
      `src: url('http://example.com/fonts/JetBrainsMono.woff2') format('woff2');`,
    );
  });
});
