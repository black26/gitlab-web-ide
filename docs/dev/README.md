# Developer docs

- [Development environment setup](./development_environment_setup.md) - Start here
- [Style Guide](./style_guide.md)
- [Architecture packages](./architecture_packages.md)
- [Working with packages](./packages.md)
- [Working with VS Code extensions](./vscode_extensions.md)
- [Developer FAQ](./faq.md)
- [VS Code Server](./vscode-server.md)
- [Fonts](./fonts.md)
- [Instrumentation](./instrumentation.md)
