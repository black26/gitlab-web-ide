# GitLab build of VS Code Server

## Connect the Web IDE to VS Code Server

The Web IDE can serve as a client for VS Code Server, which is what we call _Remote Development Web IDE_.

To connect the Web IDE to VS Code Server:

1. [Set up VS Code Server](#set-up-vs-code-server).
1. [Start the Web IDE](#start-the-web-ide).

### Set up VS Code Server

_We tried to simplify this process by [creating a docker image](https://gitlab.com/gitlab-com/create-stage/editor-poc/remote-development/gitlab-rd-web-ide-docker)._

1. Find out which VS Code version the Web IDE uses in [`packages/vscode-build/vscode_version.json`](../../packages/vscode-build/vscode_version.json) (for example, `1.71.1-1.0.0-dev-20221006100408`).
1. Download the matching server build from the [package registry](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/packages).
1. Unpack the VS Code Remote Extension Host (REH) server archive.
1. In the unpacked server folder, find `product.json` and add these parameters:

   ```json
   {
     "webEndpointUrlTemplate": "http://localhost:8000/{{quality}}-{{commit}}",
     "extensionsGallery": {
       "serviceUrl": "https://open-vsx.org/vscode/gallery",
       "itemUrl": "https://open-vsx.org/vscode/item",
       "resourceUrlTemplate": "https://open-vsx.org/vscode/asset/{publisher}/{name}/{version}/Microsoft.VisualStudio.Code.WebResources/{path}"
     }
   }
   ```

   - Set the template `webEndpointUrlTemplate` to contain the host on which the Web IDE runs (for example, `gitlab.com`). This is a workaround for CORS headers ([VS Codium server can't install extensions](https://gitlab.com/gitlab-org/gitlab/-/issues/371616)).
   - Set the `extensionGallery` attribute manually until [Configure Open VSX as the extension marketplace for our server](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/9) is completed.

1. Start the server with `./server.sh`

   ```sh
   ./bin/code-server-oss \
    --host localhost \
    --port 8081
    --telemetry-level off \
    --connection-token password
   ```

### Start the Web IDE

1. [Use the Web IDE example app](https://gitlab.com/gitlab-org/gitlab-web-ide#how-to-use-the-example).

   - Use the GitLab Pages app if you can serve the server (for example, port `8081`) over HTTPS.
   - Run the example locally if you can only serve the server over HTTP.

2. [Use the Remote Development setup for the Web IDE](https://gitlab.com/gitlab-org/gitlab-web-ide#for-remote-development-webide).

## Troubleshooting

### Version mismatch

The client and server have to be built from the exact same commit. Otherwise, you run into the [Web IDE and VS Code Server version mismatch](https://gitlab.com/gitlab-org/gitlab/-/issues/371615) issue.

The linked issue contains a description of how to work around this problem. However, because we build the server together with the web VS Code (Web IDE), there is no version mismatch problem as long as you use the same version of [server](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/packages) and [web build](../../packages/vscode-build/vscode_version.json).

For hacking with different versions of VS Code on the client and server, you can override the `commit` attribute:

- In `product.json` on the server. You can also remove `commit` from `product.json` because VS Code uses `dev` as the default commit.
- In [`vscode-bootstrap/src/start.ts` for the client](https://gitlab.com/gitlab-org/gitlab-web-ide/-/blob/8f9547afdb4ce7f673a52e6543427fdfa1bf65bd/packages/vscode-bootstrap/src/start.ts#L42) (Web IDE).
- As long as the commit is `dev` in either client or server, VS Code ignores version matching.

### Installing extensions

To install extensions, both the client and the server must configure Open VSX as the marketplace for extensions.

The client is [already configured](https://gitlab.com/gitlab-org/gitlab-web-ide/-/blob/8f9547afdb4ce7f673a52e6543427fdfa1bf65bd/packages/vscode-bootstrap/src/start.ts#L45-50). You must [manually configure](#first-setup-the-server) the server.

Because the Web IDE and VS Code Server don't run on the same origin, you can get the CORS headers issue. When the Web IDE tries to download extension assets (for example, icons), the server must send `Allow-Origin` headers, which doesn't happen automatically. A [workaround](https://gitlab.com/gitlab-org/gitlab/-/issues/371616#note_1087978299) is to use the `webEndpointUrlTemplate` attribute in `product.json` on the server.
